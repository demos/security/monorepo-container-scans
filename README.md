## Monorepo Security Scan Example

This project is an example of a monorepo containing mutliple python projects.  In this case, each project is contained in its own top level directory - `python1` and `python2`, each with its own `Dockerfile` that specifies how to build the containerized application, and each with a `requirements.txt` file that defines the packages to be used.

The `.gitlab-ci.yml` file uses hidden jobs for build, container scanning, and dependency scanning, then extends to run the appropriate job for the directory being built, specified by the `$BUILD_DIR` environment variable defined in the build job.  The $BUILD_DIR and other related environment variables is written to `imagevars.env` so that these variables cane used by each of the security scans being run for the same project.

For Container Scanning:
- `CS_IMAGE: $CI_REGISTRY_IMAGE/$PROJECT_DIR:$CI_COMMIT_SHA` - named with the name of the project directory being built, and tagged with the current commit sha
- `CS_DOCKERFILE_PATH: $PROJECT_DIR/Dockerfile` - specifies how to find the Dockerfile for each project


For monorepos with multiple scan jobs running within a single pipeline, it's important to note:

- The scans must be run as separate jobs for each component contained within the monorepo, and all components must be scanned within the same pipeline in order to guarantee that your vulnerability report contains the complete list of vulnerabilities across the repo
- The vulnerabilities found in all of the dependency scans and container scans are aggregated into the final vulnerability report - the default job report names can be used for each scan and there is no issue with multiple jobs producing the same report name

- The CycloneDX dependency reports are created for each separate component and stored in a separate project folder.  The separate reports can be downloaded as artifacts from the latest pipeline run for the default branch.  The reports are automatically aggregated into a single json report that can be downloaded from the Dependency List page


